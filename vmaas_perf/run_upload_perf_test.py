#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Perf test for updates requests.
"""
import argparse
import sys

from perflib import consts
from perflib import pkglists
from perflib import tsung


def get_args(args=None):
    """Gets command line arguments."""
    parser = argparse.ArgumentParser(description="run_upload_test")
    parser.add_argument("-i", "--packages-file", required=True, help="File with list of rpm files")
    parser.add_argument(
        "-s", "--server", required=True, action="append", help="Server hostname:port"
    )
    parser.add_argument(
        "-c",
        "--client",
        default=["localhost"],
        action="append",
        help="Client host:cpus:maxusers" " (default: %(default)s)",
    )
    parser.add_argument(
        "-d",
        "--duration",
        type=int,
        default=consts.DEFAULT_DURATION,
        metavar="SEC",
        help="Duration of test run (in seconds)" " (default: %(default)s)",
    )
    parser.add_argument(
        "-u",
        "--users-num",
        type=int,
        default=consts.DEFAULT_USERS_NUM,
        metavar="USERS",
        help="How many concurrent users" " (default: %(default)s)",
    )
    parser.add_argument(
        "-p",
        "--packages-num",
        type=int,
        default=consts.DEFAULT_PACKAGES_NUM,
        metavar="PACKAGES",
        help="How many packages per request" " (default: %(default)s)",
    )
    parser.add_argument(
        "--one-per-user",
        action="store_true",
        help="Send just one request per user per second" " (default: %(default)s)",
    )
    parser.add_argument(
        "--requests-num",
        type=int,
        default=consts.DEFAULT_REQUESTS_NUM,
        metavar="REQUESTS",
        help="How many unique requests to generate" " (default: %(default)s)",
    )
    return parser.parse_args(args)


def main(args=None):
    """Main function for cli."""
    args = get_args(args)

    # remove default value if non-default was specified
    clients = args.client
    if len(clients) > 1:
        clients = clients[1:]

    counts_list = pkglists.get_counts_list(args.packages_num, args.requests_num)
    jsons_list = pkglists.gen_jsons(args.packages_file, counts_list)

    log_path = tsung.run_test(
        jsons_list,
        tsung.get_clients(clients),
        tsung.get_servers(args.server),
        args.duration,
        args.users_num,
        "/api/v1/updates/",
        args.one_per_user,
    )
    if not log_path:
        return 2

    print(f"Log path: {log_path}")
    return 0


if __name__ == "__main__":
    sys.exit(main())
