# -*- coding: utf-8 -*-
"""
Generate package lists for VMaaS /upload.
"""
import json
import random


def load_package_list(packages_file):
    """Loads list of packages from file."""
    with open(packages_file) as pkgs:
        packages = pkgs.read().splitlines()
    return packages


def select_packages(packages, num):
    """Creates random list of packages."""
    padded = {random.choice(packages) for __ in range(num + 200)}
    return list(padded)[:num]


def gen_packages_query(packages, releasever=None):
    """Generates request body for package updates query out of list of packages."""
    body = {"package_list": list(packages)}
    if releasever:
        body["releasever"] = releasever
    return body


def gen_jsons(packages_file, counts_list, releasever=None):
    """Generates JSON files with updates requests."""
    jsons_list = []
    packages = load_package_list(packages_file)
    for i, count in enumerate(counts_list):
        selected = select_packages(packages, count)
        query = gen_packages_query(selected, releasever=releasever)
        json_file = f"updates{i}.json"
        with open(json_file, "w") as out:
            json.dump(query, out, sort_keys=True, indent=4, separators=(",", ": "))
        jsons_list.append(json_file)

    return jsons_list


def get_counts_list(packages_num, requests_num):
    """Gets list of how many packages will be used for each unique request.

    >>> get_counts_list(1000, 3)
    [1000, 1000, 1000]
    """
    return [packages_num for __ in range(requests_num)]
