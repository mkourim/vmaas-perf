# -*- coding: utf-8 -*-
"""
OpenShift APIs.
"""
import requests
from wait_for import wait_for

from .exceptions import PerfException

DELAY = 5
NUM_SEC = 120


class OpenShiftAPIs:
    """Actions using OpenShift APIs."""

    def __init__(self, api_address, auth_token, namespace):
        self.api_address = api_address.rstrip("/")
        self.auth_token = auth_token
        self.namespace = namespace
        self._cache = {}

    def _request_deploymentconfigs(self):
        address = "{}/apis/apps.openshift.io/v1/namespaces/{}/deploymentconfigs".format(
            self.api_address, self.namespace
        )
        headers = {"Authorization": f"Bearer {self.auth_token}"}

        return requests.get(address, headers=headers)

    def _scale_request(self, deploymentconfig_name, replicas_num):
        address = "{}/oapi/v1/namespaces/{}/deploymentconfigs/{}/scale".format(
            self.api_address, self.namespace, deploymentconfig_name
        )
        data = {
            "kind": "Scale",
            "metadata": {"name": deploymentconfig_name, "namespace": self.namespace},
            "spec": {"replicas": replicas_num},
        }
        headers = {"Content-Type": "application/json", "Authorization": f"Bearer {self.auth_token}"}

        return requests.put(address, json=data, headers=headers)

    def _cache_update(self, key, method, force):
        if not force and self._cache.get(key):
            return

        try:
            response = method()
        except Exception as err:
            raise PerfException(f"Failed to get {key}.") from err
        if not response:
            raise PerfException(f"Failed to get {key}.")
        self._cache[key] = response.json()

    def load_deploymentconfigs(self, force=False):
        """Loads deploymentconfigs.

        Args:
            force (bool): Reload even when already loaded.
        """
        self._cache_update("deploymentconfigs", self._request_deploymentconfigs, force)
        return self

    def get_deploymentconfig(self, partial_name):
        """Gets deploymentconfig by its (partial) name.

        Args:
            partial_name (str): The (partial) name of deploymentconfig.
        """
        self.load_deploymentconfigs()
        deploymentconfigs = self._cache.get("deploymentconfigs") or {}
        deploymentconfigs = deploymentconfigs.get("items") or ()

        found = []
        for item in deploymentconfigs:
            item_name = item.get("metadata") or {}
            item_name = item_name.get("name") or ""
            if partial_name in item_name:
                found.append(item)

        if not found:
            raise PerfException(f"No deploymentconfig matching `{partial_name}` found.")
        if len(found) > 1:
            raise PerfException(f"Multiple deploymentconfigs matching `{partial_name}` found.")
        return found[0]

    def get_deploymentconfig_name(self, partial_name):
        """Gets the full name of deploymentconfig by its partial name.

        Args:
            partial_name (str): The (partial) name of deploymentconfig.
        """
        deploymentconfig = self.get_deploymentconfig(partial_name)
        return deploymentconfig["metadata"]["name"]

    def get_replicas_num(self, name):
        """Gets number of replicas for deploymentconfig using its (partial) name.

        Args:
            name (str): The (partial) name of deploymentconfig.
        """
        deploymentconfig = self.get_deploymentconfig(name)
        replicas_num = deploymentconfig.get("status") or {}
        replicas_num = replicas_num.get("readyReplicas") or 0
        return replicas_num

    def scale_deploymentconfig(self, deploymentconfig_name, pods_num):
        """Scales replicas for deploymentconfig specified by its full name.

        Doesn't wait until scaling is finished.

        Args:
            deploymentconfig_name (str): The full name of deploymentconfig.
            pods_num (int): How many replicas are needed.
        """
        failed_msg = f"Failed to scale `{deploymentconfig_name}` to {pods_num} pods"
        try:
            response = self._scale_request(deploymentconfig_name, pods_num)
        except Exception as err:
            raise PerfException("{}: {}".format(failed_msg, str(err)))
        if not response:
            raise PerfException(f"{failed_msg}.")
        return response

    def wait_for_scaling_finished(self, name, pods_num, delay=DELAY, num_sec=NUM_SEC):
        """Waits until scaling of replicas for deploymentconfig is finished.

        Args:
            name (str): The (partial) name of deploymentconfig.
            pods_num (int): How many replicas are needed.
            delay (int): How long to wait before the query is repeated.
            num_sec (int): How long to wait for scaling finished.
        """

        def _is_finished():
            self.load_deploymentconfigs(force=True)
            deploymentconfig = self.get_deploymentconfig(name)
            status = deploymentconfig.get("status") or {}
            unavailable = status.get("unavailableReplicas")
            replicas_num = status.get("readyReplicas")
            return unavailable == 0 and replicas_num == pods_num

        wait_for(_is_finished, delay=delay, num_sec=num_sec, silent_failure=True)

        scaled_to_num = self.get_replicas_num(name)
        if pods_num != scaled_to_num:
            raise PerfException(
                "Failed to scale `{}` to {} pods, current pods num is {}.".format(
                    name, pods_num, scaled_to_num
                )
            )

        return self

    def scale_app(self, name, pods_num, delay=DELAY, num_sec=NUM_SEC):
        """Scales replicas for app specified by the (partial) name of deploymentconfig.

        Waits until scaling is finished.

        Args:
            name (str): The (partial) name of deploymentconfig.
            pods_num (int): How many replicas are needed.
            delay (int): How long to wait before the query is repeated.
            num_sec (int): How long to wait for scaling finished.
        """
        full_name = self.get_deploymentconfig_name(name)
        scale_exception_msg = None
        scale_fail_msg = f"Failed to scale `{full_name}` to {pods_num} pods"

        def _request_success():
            nonlocal scale_exception_msg
            scale_exception_msg = None

            try:
                response = self.scale_deploymentconfig(full_name, pods_num)
            except PerfException as err:
                scale_exception_msg = str(err)
                return False
            return response or False

        scale_reponse = wait_for(_request_success, delay=2, num_sec=10, silent_failure=True)
        if scale_reponse is None:
            raise PerfException(scale_exception_msg or scale_fail_msg)

        self.wait_for_scaling_finished(full_name, pods_num, delay=delay, num_sec=num_sec)

        return self

    def __repr__(self):
        return f"<OpenShiftAPIs {self.api_address} {self.namespace}>"


def get_apis_obj(conf):
    """Gets the instance of OpenShiftAPIs.

    Args:
        conf (dict): Perf configuration.
    """
    conf = conf.get("openshift") or {}
    apis_address = conf.get("api_address")
    if not apis_address:
        raise PerfException("The `api_address` is not specified in the configuration file.")

    auth_token = conf.get("auth_token")
    if not auth_token:
        raise PerfException("The `auth_token` is not specified in the configuration file.")

    namespace = conf.get("namespace")
    if not namespace:
        raise PerfException("The `namespace` is not specified in the configuration file.")

    return OpenShiftAPIs(apis_address, auth_token, namespace)
