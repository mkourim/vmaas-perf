# -*- coding: utf-8 -*-
"""
Reads OpenShift metrics.
"""
import datetime
import json
from contextlib import contextmanager

import requests
from wait_for import wait_for

from .exceptions import PerfException

DELAY = 5
NUM_SEC = 15


class OpenShiftMetrics:
    """Getting metrics from OpenShift."""

    TAGS = "descriptor_name:memory/usage|cpu/usage_rate,type:pod_container,container_name:"
    KEYS = ("avg", "max", "median", "min", "sum")

    def __init__(self, metrics_address, auth_token, namespace, container_name):
        self.metrics_address = metrics_address.rstrip("/")
        self.auth_token = auth_token
        self.namespace = namespace
        self.container_name = container_name

    def request_stats(self, duration):
        """Performs the API request.

        Args:
            duration (int): Stats from the past number of seconds.
        """
        data = {
            "tags": f"{self.TAGS}{self.container_name}",
            "bucketDuration": f"{duration}s",
            "start": f"-{duration}s",
        }
        HEADERS = {
            "Authorization": f"Bearer {self.auth_token}",
            "Content-Type": "application/json",
            "Hawkular-Tenant": self.namespace,
        }
        address = f"{self.metrics_address}/m/stats/query"

        return requests.post(address, json=data, headers=HEADERS)

    @staticmethod
    def get_usages(stats):
        """Gets usage stats.

        Args:
            stats (dict): Complete stats.
        """
        stats_gauge = stats.get("gauge")
        if not stats_gauge:
            raise PerfException("Cannot get stats, the `gauge` key is missing.")

        cpu_usage = []
        mem_usage = []
        for name, rec in stats_gauge.items():
            if "/memory/" in name:
                db = mem_usage
            elif "/cpu/" in name:
                db = cpu_usage
            else:
                continue

            for data in rec:
                if data.get("empty"):
                    continue
                db.append(data)

        return cpu_usage, mem_usage

    @staticmethod
    def aggregate_stats(cpu_usage, mem_usage, keys):
        """Aggregates stats from multiple pods.

        Args:
            cpu_usage (list of dicts): CPU usage stats.
            mem_usage (list of dicts): Memory usage stats.
            keys (iterable): List of key names whose values will be aggregated.
        """
        cpu_data = {}
        if cpu_usage:
            cpu_data.update(cpu_usage[0])
            del cpu_data["empty"]
            cpu_data["pods"] = len(cpu_usage)
        for rec in cpu_usage[1:]:
            for key in keys:
                cpu_data[key] = cpu_data[key] + rec[key]

        mem_data = {}
        if mem_usage:
            mem_data.update(mem_usage[0])
            del mem_data["empty"]
            mem_data["pods"] = len(mem_usage)
        for rec in mem_usage[1:]:
            for key in keys:
                mem_data[key] = mem_data[key] + rec[key]

        return {"memory": mem_data, "cpu": cpu_data}

    def get_stats(self, duration):
        """Collects aggregated stats.

        Args:
            duration (int): Stats from the past number of seconds.
        """
        stats_response = self.request_stats(duration)
        stats = stats_response.json()
        cpu_usage, mem_usage = self.get_usages(stats)
        return self.aggregate_stats(cpu_usage, mem_usage, self.KEYS)

    def wait_for_stats(self, duration, delay=DELAY, num_sec=NUM_SEC):
        """Waits for successfull collection of stats.

        Args:
            duration (int): Stats from the past number of seconds.
            delay (int): Wait for number of seconds before repeating the request.
            num_sec (int): Wait for number of seconds for successfull collection of stats.
        """

        def _get_stats():
            try:
                stats = self.get_stats(duration)
            except PerfException:
                return False
            return stats

        result = wait_for(_get_stats, delay=delay, num_sec=num_sec, silent_failure=True)
        if result is None:
            raise PerfException("Failed to get metrics stats.")
        return result.out

    def __repr__(self):
        return f"<OpenShiftMetrics {self.container_name}>"


def get_metrics_obj(conf, container_name):
    """Gets the instance of OpenShiftMetrics.

    Args:
        conf (dict): Perf configuration.
    """
    metrics_address = conf.get("metrics_address")
    if not metrics_address:
        raise PerfException("The `metrics_address` is not specified in the configuration file.")

    auth_token = conf.get("auth_token")
    if not auth_token:
        raise PerfException("The `auth_token` is not specified in the configuration file.")

    namespace = conf.get("namespace")
    if not namespace:
        raise PerfException("The `namespace` is not specified in the configuration file.")

    if not container_name:
        raise PerfException(f"The `container_name` cannot be `{container_name}`.")

    return OpenShiftMetrics(metrics_address, auth_token, namespace, container_name)


# pylint: disable=too-many-arguments
@contextmanager
def save_metrics(conf, container_name, starts_before=120, run_id=None, tag=None, is_openshift=True):
    """Saves aggregated metrics.

    Args:
        starts_before (int): Stats from the past number of seconds.
        tag (str): Tag used for collecting perf jobs.
        is_openshift (bool): Running on OpenShift.
    """
    if not is_openshift:
        yield
        return

    openshift_metrics = get_metrics_obj(conf.get("openshift") or {}, container_name)

    testing_start = datetime.datetime.now()
    metrics_start = openshift_metrics.wait_for_stats(starts_before)
    yield
    testing_finish = datetime.datetime.now()
    testing_duration = testing_finish - testing_start
    testing_duration = int(testing_duration.total_seconds())
    metrics_finish = openshift_metrics.wait_for_stats(testing_duration)

    metrics_db = {"start": metrics_start, "finish": metrics_finish, "duration": testing_duration}
    metrics_db["tag"] = tag
    metrics_db["run_id"] = run_id

    filename = f"metrics-{testing_finish:%Y%m%d%H%M%S}.json"
    with open(filename, "w") as out_file:
        json.dump(metrics_db, out_file, indent=4)
