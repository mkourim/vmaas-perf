# -*- coding: utf-8 -*-
"""
Generates perf tests configured in conf/perf.yaml
"""
import logging
import os
import uuid
from contextlib import contextmanager

import yaml

from . import consts


def load_conf(conf_yaml=None):
    """Loads the config file."""
    conf_yaml = conf_yaml or consts.PERF_CONF
    with open(conf_yaml, encoding="utf-8") as input_file:
        config_settings = yaml.load(input_file)
    return config_settings


def gen_run_id():
    """Generates unique run id."""
    return str(uuid.uuid4())


@contextmanager
def chdir(target_dir):
    """Context manager that changes dir to `target_dir` and back."""
    original_dir = os.getcwd()
    os.chdir(target_dir)
    yield
    os.chdir(original_dir)


def init_log(log_level):
    """Initializes logging."""
    log_level = log_level or "INFO"
    logging.basicConfig(
        format="%(name)s:%(levelname)s:%(message)s",
        level=getattr(logging, log_level.upper(), logging.INFO),
    )
