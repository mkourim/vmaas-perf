# -*- coding: utf-8 -*-
"""
Generates perf tests configured in conf/perf.yaml
"""
import abc
import datetime
import json
import logging
import os
import time

from . import consts
from . import tsung
from . import utils

# pylint: disable=invalid-name
logger = logging.getLogger(__name__)


class PerfJob(metaclass=abc.ABCMeta):
    """Job definition."""

    # pylint: disable=too-many-instance-attributes
    def __init__(self, name, data, endpoint=None):
        self.name = name
        self.description = data.get("description")
        self.duration = data.get("duration") or consts.DEFAULT_DURATION
        self.users_num = data.get("users_num") or consts.DEFAULT_USERS_NUM
        self.one_req_per_user = data.get("one_per_user") or False
        self.tags = data.get("tags") or []
        self.endpoint = data.get("endpoint") or endpoint
        self.pods_num = data.get("pods_num", 1)
        self.log_path = None
        self.start = None
        self.end = None

    @abc.abstractmethod
    def get_jsons_lists(self):
        """Returns list of JSON files."""

    def run_test(self, clients, servers):
        """Runs single test."""
        return tsung.run_test(
            self.get_jsons_lists(),
            clients,
            servers,
            self.duration,
            self.users_num,
            self.endpoint,
            self.one_req_per_user,
        )

    @staticmethod
    def run_pre():
        return True

    @staticmethod
    def run_post():
        return True

    def run(self, clients, servers):
        if not self.run_pre():
            logger.error("Failed to run 'pre' step, skipping %s", repr(self))
            return None
        self.start = int(time.time())
        retval = self.run_test(clients, servers)
        self.end = int(time.time())
        self.run_post()
        return retval

    def __repr__(self):
        return f"<PerfJob {self.name}>"


class PerfJobsRunner:
    """Run perf jobs."""

    # pylint: disable=too-many-arguments,too-many-instance-attributes
    def __init__(self, clients, servers, jobs, run_name=None, run_id=None, tag=None):
        self.clients = clients
        self.servers = servers
        self.jobs = jobs
        self.tag = tag
        self.run_name = run_name
        self.run_id = run_id or utils.gen_run_id()
        self.start = None
        self.end = None

    def write_job_name(self, job):
        """Writes the name of the job to the job log directory."""
        try:
            abspath = os.path.abspath(job.log_path)
        except TypeError:
            return
        job_name_file = os.path.join(abspath, "data", consts.JOB_INFO_FILE)
        with open(job_name_file, "w") as output_file:
            output_file.write(f"name: {job.name}\n")
            if self.run_name:
                output_file.write(f"run: {self.run_name}\n")
            if self.run_id:
                output_file.write(f"id: {self.run_id}\n")

    def run_jobs(self):
        """Runs all jobs."""
        self.start = int(time.time())

        for job in self.jobs:
            if self.tag and self.tag not in job.tags:
                continue
            job.log_path = job.run(self.clients, self.servers)
            self.write_job_name(job)

        self.end = int(time.time())

        return self

    @staticmethod
    def get_status_files(log_path):
        """Returns dict with http statuses and corresponding log files."""
        status_dir = os.path.join(log_path, "data")
        statuses = {}

        for data_file in os.listdir(status_dir):
            if not data_file.endswith(".txt"):
                continue

            try:
                status = int(data_file.replace(".txt", ""))
            except ValueError:
                continue

            statuses[status] = os.path.join(status_dir, data_file)

        return statuses

    def get_job_statuses(self, log_path):
        """Gets results of a single job."""
        status_files = self.get_status_files(log_path)
        statuses = []

        for status, status_file in status_files.items():
            with open(status_file) as input_file:
                try:
                    line = input_file.readlines()[-2]
                except IndexError:
                    continue
            duration, __, total_count = line.split(" ")
            duration, total_count = float(duration) + 10, float(total_count)
            total_avg = total_count / duration
            statuses.append((int(status), int(duration), total_avg, int(total_count)))

        return statuses

    def gen_stats(self):
        """Generates file with results from each test run."""
        stats = {"results": {}}
        results = stats["results"]
        for job in self.jobs:
            if not job.log_path:
                continue
            log_path = os.path.abspath(job.log_path)
            job_statuses = self.get_job_statuses(log_path)
            if not job_statuses:
                continue
            tsung_id = os.path.split(log_path.rstrip("/"))[1]
            results[job.name] = {
                "tsung_id": tsung_id,
                "start": job.start,
                "end": job.end,
                "statuses": {},
            }
            statuses = results[job.name]["statuses"]
            for status, duration, total_avg, total_count in job_statuses:
                statuses[status] = {
                    "duration": duration,
                    "total_avg": total_avg,
                    "total_count": total_count,
                }

        stats["tag"] = self.tag
        stats["run_name"] = self.run_name
        stats["run_id"] = self.run_id
        stats["start"] = self.start
        stats["end"] = self.end

        filename = "run-{:%Y%m%d%H%M%S}.json".format(datetime.datetime.now())
        with open(filename, "w") as out_file:
            json.dump(stats, out_file, indent=4)

        return self

    def __repr__(self):
        return f"<PerfJobsRunner {self.run_id}>"


def get_jobs_data(conf):
    """Collects all jobs defined in the config file."""
    for job_name, job_data in conf.items():
        if "job_" in job_name:
            yield job_name, job_data
