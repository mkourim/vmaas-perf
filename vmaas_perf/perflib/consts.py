# -*- coding: utf-8 -*-
"""
Constants.
"""
import os

PERF_CONF = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..", "perf.yaml")

DEFAULT_DURATION = 600
DEFAULT_USERS_NUM = 100

JOB_INFO_FILE = "perf_job.txt"

# VMaaS specific
DEFAULT_REQUESTS_NUM = 20
DEFAULT_PACKAGES_NUM = 1000
