# -*- coding: utf-8 -*-
"""
Tsung wrapper.
"""
import collections
import re
import subprocess

from lxml import etree

from .exceptions import PerfException
from .utils import chdir

LOG_RE = re.compile(r"Log directory is: (.*)")

Client = collections.namedtuple("Client", "host cpus maxusers")
# cpus and maxusers are optional
Client.__new__.__defaults__ = (1, 600)  # type: ignore

Server = collections.namedtuple("Server", "host port")
# port defaults to 80
Server.__new__.__defaults__ = ("default",)  # type: ignore


class TsungTest:
    """Wrapper for running Tsung."""

    TSUNG_XML = "tsung.xml"

    # pylint: disable=too-many-arguments
    def __init__(
        self, jsons_list, clients, servers, duration, users_num, endpoint, one_req_per_user=False
    ):
        self.jsons_list = jsons_list
        self.clients = clients
        self.servers = servers
        self.duration = duration
        self.users_num = users_num
        self.endpoint = endpoint
        self.one_req_per_user = one_req_per_user

    @staticmethod
    def _top_element():
        """Creates top XML element."""
        top = etree.Element("tsung", {"loglevel": "warning"})
        return top

    def _add_clients(self, parent_element):
        """Adds clients section to XML."""
        client_element = etree.SubElement(parent_element, "clients")
        for host, cpu, maxusers in self.clients:
            attrs = {"host": host, "cpu": str(cpu), "maxusers": str(maxusers)}
            if host == "localhost" and cpu == 1:
                attrs["use_controller_vm"] = "true"
            etree.SubElement(client_element, "client", attrs)

    def _add_servers(self, parent_element):
        """Adds servers section to XML."""
        servers_element = etree.SubElement(parent_element, "servers")
        for host, port in self.servers:
            port = str(port)
            conn_type = "tcp"
            if "https://" in host:
                conn_type = "ssl"
            if port == "default":
                port = "443" if conn_type == "ssl" else "80"
            host = re.sub("^https?://", "", host)
            etree.SubElement(
                servers_element, "server", {"host": host, "port": port, "type": conn_type}
            )

    def _add_load(self, parent_element):
        """Adds load section to XML."""
        load_element = etree.SubElement(
            parent_element, "load", {"duration": str(self.duration), "unit": "second"}
        )
        phase_element = etree.SubElement(
            load_element,
            "arrivalphase",
            {"phase": "1", "duration": str(self.duration + 10), "unit": "second"},
        )

        users_data = {"arrivalrate": str(self.users_num), "unit": "second"}
        if not self.one_req_per_user:
            users_data["maxnumber"] = str(self.users_num)
        etree.SubElement(phase_element, "users", users_data)

    def _add_sessions(self, parent_element):
        """Adds sessions section to XML."""
        sessions_element = etree.SubElement(parent_element, "sessions")
        for i, json_file in enumerate(self.jsons_list):
            session_element = etree.SubElement(
                sessions_element,
                "session",
                {"type": "ts_http", "weight": "1", "name": f"request{i}"},
            )

            if self.one_req_per_user:
                requests_parent = session_element
            else:
                for_element = etree.SubElement(
                    session_element, "for", {"from": "1", "to": "2", "var": "i", "incr": "0"}
                )
                requests_parent = for_element

            request_element = etree.SubElement(requests_parent, "request")
            etree.SubElement(
                request_element,
                "http",
                {"url": self.endpoint, "method": "POST", "contents_from_file": json_file},
            )

    def _write_tsung_xml(self, xml_tree):
        """Writes tsung config."""
        tsung_xml = (
            b'<?xml version="1.0" encoding="utf-8"?>\n'
            b'<!DOCTYPE tsung SYSTEM "/usr/share/tsung/tsung-1.0.dtd" []>\n'
            + etree.tostring(xml_tree, pretty_print=True, encoding="utf-8")
        )
        with open(self.TSUNG_XML, "wb") as tsung_config:
            tsung_config.write(tsung_xml)

    def gen_tsung_xml(self):
        """Generates tsung config."""
        top_element = self._top_element()
        self._add_clients(top_element)
        self._add_servers(top_element)
        self._add_load(top_element)
        self._add_sessions(top_element)
        self._write_tsung_xml(top_element)
        return self

    @staticmethod
    def gen_graphs(log_path):
        """Generates tsung graphs."""
        with chdir(log_path):
            subprocess.run(
                ["tsung_stats", "--stats", "tsung.log"],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                check=True,
            )
        return log_path

    def execute_tsung(self):
        """Runs tsung process."""
        ret = subprocess.run(
            ["tsung", "-f", self.TSUNG_XML, "-l", "./", "start"],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            check=True,
        )
        return ret

    @staticmethod
    def get_log_path(ret):
        """Returns log path of tsung run."""
        log_path_re = LOG_RE.search(ret.stdout.decode("utf-8"))
        return log_path_re.group(1) if log_path_re else None

    def run_test(self):
        """Runs testing."""
        self.gen_tsung_xml()
        ret = self.execute_tsung()

        log_path = self.get_log_path(ret)
        if not log_path:
            return None

        return self.gen_graphs(log_path)


def _get_objs_list(klass, data):
    objs_list = []
    if isinstance(data, str):
        data = [data]
    for rec in data:
        if ":" in rec:
            args = rec.split(":")
            objs_list.append(klass(*args))
        else:
            objs_list.append(klass(rec))
    return objs_list


def get_servers(servers):
    """Gets list of server data."""
    objs_list = []
    if isinstance(servers, str):
        servers = [servers]
    for rec in servers:
        if ":" in rec:
            args = rec.split(":")
            # preserve http:// in host's name
            if "http" in args[0]:
                args[1] = "{}:{}".format(args[0], args[1])
                args = args[1:]
            objs_list.append(Server(*args))
        else:
            objs_list.append(Server(rec))
    return objs_list


# pylint: disable=too-many-arguments
def instantiate_tsung_test(
    jsons_list, clients, servers, duration, users_num, endpoint, one_req_per_user=False
):
    """Instantiates TsungTest."""
    if not jsons_list:
        raise PerfException("Invalid list of JSON files with requests.")
    if not clients:
        raise PerfException("Clients cannot be empty.")
    if not servers:
        raise PerfException("Servers cannot be empty.")
    if not duration:
        raise PerfException("Invalid duration.")
    if not users_num:
        raise PerfException("Invalid number of users.")
    if not endpoint:
        raise PerfException("Endpoint must be specified.")
    return TsungTest(jsons_list, clients, servers, duration, users_num, endpoint, one_req_per_user)


def get_clients(clients):
    """Gets list of clients data."""
    return _get_objs_list(Client, clients)


# pylint: disable=too-many-arguments
def run_test(jsons_list, clients, servers, duration, users_num, endpoint, one_req_per_user=False):
    """Runs testing."""
    tsung_test = instantiate_tsung_test(
        jsons_list, clients, servers, duration, users_num, endpoint, one_req_per_user
    )
    return tsung_test.run_test()
