#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# pylint: disable=c-extension-no-member
"""
Plot graphs out of results.
"""
import argparse
import os
import re
import sys
from collections import defaultdict

import matplotlib.pyplot as plt
import numpy as np
from lxml import etree
from perflib import utils

HTTP_SUCCESS = 200
RE_PODS_NUM = re.compile(r"[a-zA-Z]_+_(\d+)_")
RE_HTTP_STATUS = re.compile(r".+-(\d+)\.png")
RE_DIGITS = re.compile(r"(\d+)")
RE_JOB_NAME = re.compile(r"(.+)-\d+\.png")


def _rec_dd():
    return defaultdict(_rec_dd)


def parse_date(date):
    """Parses unseparated date string (e.g. 201807231629) into iso8601-like format."""
    year = date[:4]
    month = date[4:6]
    day = date[6:8]
    hour = date[8:10]
    minute = date[10:12]
    return f"{year}-{month}-{day}T{hour}:{minute}"


def get_barlist(y_pos, results):
    """Returns barlist representation."""
    if len(results) == 1:
        # if there's a single value, arrange the graph so the bar is on top,
        # not spread through width of the whole graph
        __, axis = plt.subplots(1, 1)
        barlist = axis.barh(y_pos, results, align="center", alpha=0.5)
        axis.set_ybound(-6.0, 0.65)
    else:
        barlist = plt.barh(y_pos, results, align="center", alpha=0.5)
    return barlist


def _get_min_max_idx(results):
    min_results = [r for r in results if r > 0.0]
    if not min_results:
        return None, None
    min_val = min(min_results)
    min_idx = [i for i, j in enumerate(results) if j == min_val]
    max_val = max(results)
    max_idx = [i for i, j in enumerate(results) if j == max_val]
    return min_idx, max_idx


def color_bars(barlist, http_status, results):
    """Sets colors of min and max values depending of http status."""
    min_idx, max_idx = _get_min_max_idx(results)
    if min_idx is None:
        return
    max_color = "g" if http_status == HTTP_SUCCESS else "r"
    min_color = "r" if http_status == HTTP_SUCCESS else "g"
    results_len = len(results)
    if len(min_idx) != results_len or http_status != HTTP_SUCCESS:
        for idx in min_idx:
            barlist[idx].set_color(min_color)
    if len(max_idx) != results_len or http_status != HTTP_SUCCESS:
        for idx in max_idx:
            barlist[idx].set_color(max_color)


def save_graph_image(job_name, http_status):
    """Saves graph image into file."""
    image_name = f"{job_name}-{http_status}.png"
    try:
        os.remove(image_name)
    except FileNotFoundError:
        pass
    plt.tight_layout()
    plt.savefig(image_name)


def plot_job_graphs(job_name, job_description, job_results, run_names):
    """Plots graph for single job."""
    for http_status in sorted(job_results.keys()):
        status_results = job_results[http_status]

        if http_status != HTTP_SUCCESS:
            run_dates = sorted(job_results[HTTP_SUCCESS].keys())
        else:
            run_dates = sorted(status_results.keys())
        results = [status_results.get(name, 0) for name in run_dates]

        y_pos = np.arange(len(run_dates))
        y_ticks = [run_names.get(date) or parse_date(date) for date in run_dates]

        plt.clf()
        barlist = get_barlist(y_pos, results)
        color_bars(barlist, http_status, results)

        plt.yticks(y_pos, y_ticks, rotation=21)
        plt.xlabel("responses / sec")
        plt.title(
            "{}\nstatus: {}".format(job_description or job_name, http_status),
            loc="left",
            color="black" if http_status == HTTP_SUCCESS else "grey",
        )

        save_graph_image(job_name, http_status)


def _get_job_description(job_name, config):
    job_data = config.get(job_name)
    if not job_data:
        return None

    job_description = job_data.get("description")
    if not job_description:
        pods_desc = (
            "{} pod(s), ".format(job_data.get("pods_num")) if job_data.get("pods_num") else ""
        )
        job_description = "{}{} user(s), {} packages".format(
            pods_desc, job_data.get("users_num"), job_data.get("packages_num")
        )
        if not job_data.get("one_per_user"):
            job_description = f"{job_description}, max requests"

    return job_description


def plot_graphs(results_db, config):
    """Plots graphs for all jobs."""
    for job_name, job_results in results_db["results"].items():
        job_description = _get_job_description(job_name, config)
        plot_job_graphs(job_name, job_description, job_results, results_db["run_names"])


def read_results(job_file_path):
    """Reads results from job file."""
    with open(job_file_path, "r") as input_file:
        return input_file.readlines()


def get_results_db():
    """Produces database with results.

    E.g.
    results_db = {
        "results": {
            "job_1_20_1000_300_max": {
                "200": {"20180730154825": 25, "20180801134242": 28}
        },
        "run_names": {
            "20180730154825": "release 0.2"
        }
    }
    """
    results_db = {"results": _rec_dd(), "run_names": {}}
    outcome = results_db["results"]
    run_names = results_db["run_names"]

    for item_name in os.listdir():
        if not os.path.isfile(item_name):
            continue
        if not (item_name.startswith("job-") and item_name.endswith(".txt")):
            continue
        results = read_results(item_name)
        if not results:
            continue

        date = item_name[4:-4]

        result_split = results[0].split(" ")
        try:
            # check if 6th item is there and it is integer
            int(result_split[5].strip())
        except (ValueError, IndexError):
            # ... if not, the first line is a run name
            run_names[date] = results[0].strip()
            del results[0]

        for result in results:
            # pylint: disable=unused-variable
            job_name, tsung_id, status, time, total_avg, total_count = [
                s.strip() for s in result.split(" ")
            ]
            status = int(status)
            total_avg = float(total_avg)
            total_count = int(total_count)
            outcome[job_name][status][date] = total_avg
        if not outcome[job_name][HTTP_SUCCESS][date]:
            outcome[job_name][HTTP_SUCCESS][date] = 0.0

    return results_db


def get_images():
    """Gets sorted list of graph images."""
    images = []

    for item_name in os.listdir():
        if not os.path.isfile(item_name):
            continue
        if not (item_name.startswith("job_") and item_name.endswith(".png")):
            continue
        images.append(item_name)

    images.sort(key=natural_keys)
    return images


def atoi(text):
    """Converts text to int if it's a integral number."""
    return int(text) if text.isdigit() else text


def natural_keys(text):
    """Sorts in human order.

    http://nedbatchelder.com/blog/200712/human_sorting.html
    """
    return [atoi(c) for c in RE_DIGITS.split(text)]


def _process_images_queue(images_queue, element, last_http_status):
    if not images_queue:
        return
    # insert newline before and after graph group if there are http errors
    if last_http_status and last_http_status != HTTP_SUCCESS and element[-1].tag != "br":
        etree.SubElement(element, "br")
    for img in images_queue:
        etree.SubElement(element, "img", src=img)
    if last_http_status and last_http_status != HTTP_SUCCESS:
        etree.SubElement(element, "br")
    images_queue[:] = []


def insert_graphs_to_html(element, images_list):
    """Inserts graph images into html page."""
    last_pod_num = None
    last_http_status = None
    last_job_name = None
    images_queue = []
    for img in images_list:
        # sections by number of pods
        pods_num = RE_PODS_NUM.match(img)
        pods_num = pods_num and pods_num.match(img).group(1)
        if pods_num and pods_num != last_pod_num:
            _process_images_queue(images_queue, element, last_http_status)
            section = etree.SubElement(element, "h1")
            section.text = f"{pods_num} pods"
            last_pod_num = pods_num

        # group graphs by job names and delimit groups by newlines
        # if there are any http errors in that group
        job_name = RE_JOB_NAME.match(img).group(1)
        if last_job_name != job_name:
            last_job_name = job_name
            _process_images_queue(images_queue, element, last_http_status)

        last_http_status = int(RE_HTTP_STATUS.match(img).group(1))
        images_queue.append(img)


def gen_graphs_html(images_list):
    """Generates 'graphs.html' file."""
    html = etree.Element("html")
    head = etree.SubElement(html, "head")
    title = etree.SubElement(head, "title")
    title.text = "Performance Graphs"
    body = etree.SubElement(html, "body")
    insert_graphs_to_html(body, images_list)

    graphs_html = b"<!DOCTYPE html>\n" + etree.tostring(html, pretty_print=True)
    with open("graphs.html", "wb") as outfile:
        outfile.write(graphs_html)


def get_args(args=None):
    """Gets command line arguments."""
    parser = argparse.ArgumentParser(description="plot_graphs")
    parser.add_argument("-r", "--results-dir", default="./", help="Directory with tests results")
    parser.add_argument("-o", "--output-dir", default="./", help="Directory for saving graphs")
    parser.add_argument(
        "--html-only",
        action="store_true",
        help="Generates graphs.html out of existing graph files.",
    )
    return parser.parse_args(args)


def main(args=None):
    """Main function for cli."""
    args = get_args(args)

    if args.html_only:
        with utils.chdir(args.output_dir):
            images_list = get_images()
            gen_graphs_html(images_list)
        return 0

    conf = utils.load_conf()

    with utils.chdir(args.results_dir):
        results_db = get_results_db()
    with utils.chdir(args.output_dir):
        plot_graphs(results_db, conf)
        images_list = get_images()
        gen_graphs_html(images_list)

    return 0


if __name__ == "__main__":
    sys.exit(main())
