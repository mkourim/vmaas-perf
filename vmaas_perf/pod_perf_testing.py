#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Runs perf tests configured in conf/perf.yaml
"""
import argparse
import logging
import sys

from perflib import consts
from perflib import openshift_apis
from perflib import openshift_metrics
from perflib import perf_jobs
from perflib import pkglists
from perflib import tsung
from perflib import utils
from perflib.exceptions import PerfException

# pylint: disable=invalid-name
logger = logging.getLogger(__name__)

WEBAPP_CONTAINER = "vmaas-webapp"


class VMaaSPerfJob(perf_jobs.PerfJob):
    """VMaaS job definition."""

    APP_NAME = "webapp"
    PACKAGES_RELEASEVER = "7.4"
    SCALE_WAIT_TIME = 500

    # pylint: disable=too-many-arguments
    def __init__(self, name, data, packages_file, endpoint=None, os_apis=None):
        super().__init__(name, data, endpoint=endpoint)
        self.counts_list = pkglists.get_counts_list(
            data.get("packages_num") or consts.DEFAULT_PACKAGES_NUM,
            data.get("requests_num") or consts.DEFAULT_REQUESTS_NUM,
        )
        self.packages_file = packages_file
        self.os_apis = os_apis
        self.releasever = (
            data.get("releasever") if "releasever" in data else self.PACKAGES_RELEASEVER
        )

    def get_jsons_lists(self):
        return pkglists.gen_jsons(self.packages_file, self.counts_list, releasever=self.releasever)

    def run_pre(self):
        """Scales to number of pods specified for the job."""
        if not self.os_apis:
            return True
        try:
            self.os_apis.scale_app(self.APP_NAME, self.pods_num, num_sec=self.SCALE_WAIT_TIME)
        except PerfException as err:
            logger.error(str(err))
            return False
        return True


def get_args(args=None):
    """Gets command line arguments."""
    parser = argparse.ArgumentParser(description="pod_perf_testing")
    parser.add_argument("-i", "--packages-file", required=True, help="File with list of rpm files")
    parser.add_argument("-n", "--run-name", help="Name of the test run")
    parser.add_argument(
        "-t",
        "--tag",
        default=None,
        metavar="TAG",
        help="Run only jobs tagged with `tag` (default: %(default)s)",
    )
    parser.add_argument("--log-level", help="Set logging to specified level")
    return parser.parse_args(args)


def main(args=None):
    """Main function for cli."""
    args = get_args(args)
    utils.init_log(args.log_level)
    conf = utils.load_conf()

    server = conf.get("server")
    if not server:
        print(
            f"The `server` is not configured in the `{consts.PERF_CONF}` config file",
            file=sys.stderr,
        )
        return 1

    clients = tsung.get_clients(conf.get("clients") or ["localhost"])
    servers = tsung.get_servers(server)
    is_openshift = not ("localhost" in server or "127.0.0" in server)
    os_apis = openshift_apis.get_apis_obj(conf) if is_openshift else None

    jobs = []
    for job_name, job_data in perf_jobs.get_jobs_data(conf):
        jobs.append(
            VMaaSPerfJob(
                job_name, job_data, args.packages_file, endpoint="/api/v1/updates/", os_apis=os_apis
            )
        )
    # sort by pods_num from highest to lowest number
    jobs.sort(key=lambda x: x.pods_num, reverse=True)

    run_id = utils.gen_run_id()
    process_jobs = perf_jobs.PerfJobsRunner(
        clients, servers, jobs, run_name=args.run_name, run_id=run_id, tag=args.tag
    )

    try:
        with openshift_metrics.save_metrics(
            conf, WEBAPP_CONTAINER, run_id=run_id, tag=args.tag, is_openshift=is_openshift
        ):
            process_jobs.run_jobs()
    finally:
        process_jobs.gen_stats()

    return 0


if __name__ == "__main__":
    sys.exit(main())
